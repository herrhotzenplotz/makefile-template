# Declare the list of programs
PROGS				=	foo

# These and LDFLAGS can be overwritten
CFLAGS				=	-std=iso9899:1999
CFLAGS_amd64-freebsd-clang	=	-pedantic
CPPFLAGS			=	-D_XOPEN_SOURCE=600

# List the source files for each binary to be built
foo_SRCS			=	examples/foo.c

# Leave this undefined if you don't have any manpages that need to be
# installed.
MAN				=	docs/foo.8

# Declare a list of packages to be searched for and linked against
# (pkg-config).
LIBADD				=	libcurl

# Include the rules to build your program
# Important: the autodetect.sh script needs to be in place
include default.mk
